#!/bin/sh

# varem olgu tehtud muundur!
# kas 1)
# fst tegemiseks süsteemiaknas:
# hfst-xfst
# source silbita.xfscript
# save stack silbitaja.hfst

# või 2)
# kompileeri  silbitaja muundur
# echo 'save stack silbitaja.hfst' | hfst-xfst -l silbita.xfscript
# echo 'save stack jutukate_silbitaja.hfst' | hfst-xfst -l silbita_jutukad.xfscript

# 1) morf analüüs Filosofti analüsaatoriga
# 2) silbitamine hfst muunduriga

# std sisend 
# - utf-8 sõneloend, tühik eraldajaks
# std väljundis
# - valjundis tähistab liitsõnasisest sõnapiiri _ ja silbipiiri .

grep '^[abcdefghijklmnopqrstuvwõäöüxyšžzABCDEFGHIJKLMNOPQRSTUVWÕÄÖÜXYZšžŠŽč-]*$' \
\
| ./vmeta --plaintext --dontguess --dontguesspropnames --stem --fs --dontaddphonetics --path .\
\
|sed 's/ \([^ ]\)/ \l\1/g' \
\
| sed '/####/s/    ####//' \
| sed '/    /s/^[^ ]*    / /' \
| sed 's/ \/\/_[^\/]*\/\///g' \
| sed 's/+0*//g' \
| sed 's/\([aeiouõäöü]\)=\(is[tm]\)/\1.\2/g' \
| sed 's/\([aeiouõäöü]\)<\(is[tm]\)/\1.\2/g' \
| sed 's/\([aeiouõäöü]\)?\(is[tm]\)/\1.\2/g' \
| sed 's/=//g' \
| sed 's/$/ /' \
| sed 's/ \([^ ]*\) .* \1 / \1 /' \
| sed 's/ \([^ ]*\) .* \1 / \1 /' \
| sed 's/ \([^ ]*\) .* \1 / \1 /' \
| sed 's/ \([^ ]*\) .* \1 / \1 /' \
| sed '/ ise_enes/s/ iseenes[^ ]* //' \
| tr -s ' ' \
| sed 's/^ //' \
| sed 's/ $//' \
\
| sed 's/^\([^ ]*\)_\([^ _]*\) \1\2$/\1\2/' \
| sed 's/^\([^ ]*\)\([^ _]*\) \1_\2$/\1\2/' \
\
| sed 's/^\([^ ]*\)se_\([^ ]*\) \1s_e\2$/\1s_e\2/' \
| sed 's/^\([^ ]*\)s_e\([^ ]*\) \1se_\2$/\1s_e\2/' \
\
| sed 's/^\([^ ]*\)\([aeiu]\)_\([^ ]*\) \1_\2\3$/\1\2_\3/' \
| sed 's/^\([^ ]*\)_\([aeiu]\)\([^ ]*\) \1\2_\3$/\1\2_\3/' \
\
| sed 's/^\([^ ]*\)_sal\([^ ]*\) \1s_alu\2$/\1_sal\2/' \
| sed 's/^\([^ ]*\)s_alu\([^ ]*\) \1salu_\2$/\1_sal\2/' \
\
| sed 's/ .*$//' \
| sed 's/./\l&/g' \
\
| hfst-lookup -q silbitaja.hfst \
| cut -f 2 \
| tr -s '\n' \
