#!/usr/bin/env python3

"""
# command line script
./create_venv.sh
venv_elg_estnltk/bin/python3 ./docker_elg_syllablesator.py --json='{"type":"text","content":"Koer leidis kondi."}'

docker build -t tilluteenused/est_elg_syllablesator:2022.08.24 .
docker login -u tilluteenused
docker push tilluteenused/est_elg_syllablesator:2022.08.24
docker run -p 7000:7000 tilluteenused/est_elg_syllablesator:2022.08.24
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' localhost:7000/process

"""

from typing import Dict
import sys
import json
import subprocess

from elg import FlaskService
from elg.model import TextRequest, TextsResponse

def syllablesator(sisendsoned: str):
    """Silbita sisendsõned

    Args:
        content (str): Sisendtekst

    Returns:
        _type_: ELG'le vastava märgendusega json

    {
        "type":"texts",
        "texts":
        [
        {
            "content":str, // silbitatav sõne
            "features":
            {
                "syllables":str // silbitatud sõne, liitsõnapiir: '_', silbipiir '.'
                "warning":str // kui "syllables"=="" ja "warning"!="", siis ei saanud silbitada
            }
        }
        ]
    }
    """

    path = '.'
    sisendsoned = sisendsoned.replace(' ', '\n')
    proc = subprocess.Popen([f'{path}/morfi_ja_silbita.sh'], text=True,
        stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    
    proc.stdin.write(sisendsoned)
    proc.stdin.close()
    sisendsoned = sisendsoned.splitlines()
    silbitatud = proc.stdout.read().splitlines()

    i = o = 0
    texts=[]
    while i < len(sisendsoned):
        if o >= len(silbitatud):
            texts.append({"content":sisendsoned[i], "features":{"syllables":"", "warning":"pole silbitatud"}})
        else:
            x = silbitatud[o].replace('.', '').replace('_', '')
            if sisendsoned[i].casefold() != x.casefold():
                texts.append({"content":sisendsoned[i], "features":{"syllables":"", "warning":"pole silbitatud"}})
            else:
                texts.append({"content":sisendsoned[i], "features":{"syllables":silbitatud[o]}})
                o += 1                
        i += 1    
    return texts


class SYLLABLESATOR(FlaskService):
    def process_text(self, request) -> TextsResponse:
        """
        Find sentences and tokens
        :param content: {TextRequest} - input text in ELG compatible format
        :return: {TextsResponse} - CG syntax in ELG compatible format
        """
        silbitatud = syllablesator(request.content)
        return TextsResponse(texts=silbitatud)

flask_service = SYLLABLESATOR("SYLLABLESATOR")
app = flask_service.app


def run_test(my_query_str: str) -> Dict:
    """
    Run as command line script
    :param my_query_str: input in json string
    """
    print("DB:", str)
    my_query = json.loads(my_query_str)
    service = SYLLABLESATOR("SYLLABLESATOR")
    request = TextRequest(content=my_query["content"])
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True?
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_server() -> None:
    """Run as flask webserver"""
    app.run()

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='ELG compatible json')
    args = argparser.parse_args()
    if args.json is not None:
        json.dump(run_test(args.json), sys.stdout, indent=4)
    else:
        run_server()
